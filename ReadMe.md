
Animation Event Tool

Usage Documentation

![](https://lh3.googleusercontent.com/yR95Q7FHrOxO6kI3EO-ibKRvLNf9Dgcp1NIZFJoEF2ZCUBvXVMv_J7bf5UF3vszfZ6nQDc_Zr0mRLnRYqBc-wGlZrl5eALCejRls6miovypv3H-UvSz4ImEhHDETbjoaeFFBsps)

1.  ## Purpose:
    

-   The tool helps handling animation events far more easier than vanilla version from Unity interface:
    

-   Support clean and flexible timeline bar with dragging capability.
    
-   Event list can be edited directly without need of any popup
    
-   Preview window helps users quickly find the expected frame to put the event in.
    

-   Support layer files that can be merge into FBX meta file later. This process prevent conflicts from constant editing on the same FBX file by multiple user.
    

  

2.  ## Usage:
    

a.  Opening model file:
    

-   Open tool windows by selecting Windows Animation Clip Event Editor on Unity menu bar.![](https://lh3.googleusercontent.com/XWsfOVJurKEdgKk6VlqHoELIphSUQf79WvKI-a5LTo2Q7qCaBG5cpBxM7SRpxohxtaT_KJn7bG_-ii4vIG_z8kkTTON6ZtRdPyItUuPvRlSO4zAPkjTm6WA6l-1yXiIiy54mIMo)
    

  

-   Select model file that need editing by dragging & dropping it into FBX Asset file field or choosing it thought Select GameObject popup window of that field. (Click on the circle button next to the FBX Asset file field).![](https://lh4.googleusercontent.com/kj7qRCaeYCw9PZwy6VNV6VlPXSwT-CZcz3rZ0EP-4-wQ9FyPK9SvyTR-OA8kF1aBLKI2QEb7aaioc8klCHSd5YqgnGJqpW5lazYvcaHEYZr3Rkg-LB8ZXsiOUaGL_5ces-VERLo)
    
-   The animation list of the model file will be shown on the panel below, as you click on any animation, the respective event will be shown on the lowest list. The timeline bar also shows the events available on selected animation. ![](https://lh3.googleusercontent.com/yR95Q7FHrOxO6kI3EO-ibKRvLNf9Dgcp1NIZFJoEF2ZCUBvXVMv_J7bf5UF3vszfZ6nQDc_Zr0mRLnRYqBc-wGlZrl5eALCejRls6miovypv3H-UvSz4ImEhHDETbjoaeFFBsps)
    
-   The preview window also available on Inspector tab.
    

Note: To display preview windows, this tool creates dummy object on the scene, this tool will be automatically removed when the tool window is closed or the model is deselected on FBX Asset field. Be sure to make those step (or manually delete __Preview object__ out of the scene) before saving the scene.![](https://lh6.googleusercontent.com/l1e_PhnIdmnpAk4YtKYYckSZTA2l58RV5ophIrqi1Kl4BsEhlHl1lEdGafSFX0FO7O7RSdVjYG8E0EY2YdArTGUYBnWQ3V8j6XAOek_eWp7w2KVmd6k6rEsV_Qw8jHOX4MTKeSc)

b.  Creating the event:
    

-   Press the Add button below the animation list. The new event will be added under the event list. It can be edited as any other events.
    
-   The pin on timeline can be used for determining the time of the newly created event. Be sure to drag it to desired frame before clicking Add button.![](https://lh6.googleusercontent.com/lUAGFwtLE7AFpftCIpafMbvivL-BYZjcc8XPSmQy5Owb8QipVBeGDWy6NXMUqBTvEJp1E5cCjxSbmkIq-_F0LGQ414fvqB8LIdfLsPMKl4-_-d3mxGbzfRBuAyAH8VhU2zbd2f4)
    

c.  Editing & Delete the event:
    

-   The list of event below the timeline is designed for direct edit based on input field on each event row.
    
-   On timeline bar, each event represented by a green block on below line. You can select those blocks for highlighting the respective event on the list below. On the other hand, dragging those blocks will change the time object the event respectively, and vice versa.
    
-   To delete the event, press the X button on the rightmost of that event row.![](https://lh4.googleusercontent.com/XRjW4CWJnQwTAgy6dotn9kG-EfXzjLcTwm_8Yz5nzstCH8PsbwzjCHgbYV7W6LwTTDyqYiBOYppfGD1ex3K9oC8xXErSbniY_R52elqTGz5_vWzAp8a46sg3_zbIU_2YAC_PcKw)
    

d.  Saving the event:
    

-   After done editing, press the Save button on the bottom of the windows to save changes on the selected animation.
    

Note:  The tool won’t support auto-save feature for poorly performance on model file. So be sure to save often.![](https://lh4.googleusercontent.com/iWK9GFcW3N38gNDS8hLbPtxqJOMOZhMfGC4G32XLLdm0_TBp93TD1y8poioENBFxs1oRWxVZKO6w_frpnLsTtR3iOeh6ZEffEJop9CnvyyGdiKdsEsYksu4a9sOtynPzxtdMS7c)

e.  Using layer file feature:
    

-   This feature lets user edits animation events on separated layer files. When the editing process is done, one user can merge all the layer files into final model file, those layer file will be removed after this action.
    

f.  Add/Select/Remove the layer files:
    

-   After choosing the model file (step a), you can create the new event file by click on Add button on the right side of the Event file line, below FBX Asset line.![](https://lh5.googleusercontent.com/Fqe6iemPAF3ihwxcHg-N-Zm0OPkHcXo0uCGSK1c_Qa-60e4lZzpbHyuzKLUNp4v5Y-nx3ETAPyEjZ1Kyjshtcel2oeexB0gWoYmOReIUYoIpRkwdchqkp6EayevgkQylU02Sn0o)
    
-   The Save file window appears, you can save as a new file or overwrite another file.
    

Note: All the layer will be saved and read at Resources/AnimationEventFiles/, you can handle those files manually. The tool supports subdirectory.

-   To select a layer file, drop down the Event file list and select your layer, now you are free to edit the events from the layer file. The first entry of the list “On Prefab” means you are editing the model file directly.![](https://lh5.googleusercontent.com/m20WRMcPZgTfCWphsnz_PloIuqlQo8ESHrX3sKStCqaQ-jA6OMKMdzf3G4xTehqsC7y-9WpzMHorMDNOBHCdWgxc7dNgR4VuHVl0v237eLlSFcdXRg2-bfja36myi0LIj1YqVnc)
    
-   To Delete layer file, click the Remove button when selecting that layer. The layer file will physically deleted.
    

g  Merge the layer files into main model files:
    

-   The tool provides 2 merge tools on the bottom left of the windows:
    

Merge current file: only merge the events the current layer file to current model. The file will be deleted after this process.

Merge all files: merge all the layer files the linked to current model. Those files will be deleted after this process.

  

3.  ## Known issues:
    

a.  Red error after saving event on prefab or merging layer files:![](https://lh5.googleusercontent.com/k8t1xtQAG-iWFngDKODuc1nrvecSzWDRRfTsKIa3uWJDXBIkIqo_d-kBFQRpYPtM4nMNRJJV4a-_joB0j8QRMgLHODSMrBcVhaiYH_mp0qExdfxeKRw2NNybMVH9bGoOjM6T-3U)
    

-   This issue caused by some deep invoke into Unity DLL. It was tested and verified that won’t cause any problems to current tool and scene.
    

b.  Losing object on preview window
    

-   This happens when the user try to select another gameobject instead of preview one. Sometimes it caused by failed invoking Unity DLL functions.
    
-   Solution: Click on Reload button for the tool reloading the model and the preview window refocusing on previewed object.