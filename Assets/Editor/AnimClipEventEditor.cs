﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class AnimationProperties
{
	public readonly int Index;
	public readonly string Name;
	public readonly float Length;
	public AnimationEvent[] AnimationEvents;
	
	public float procTime = 0;
	public AnimationProperties(string Name, int Index, float Length, AnimationEvent[] animEvent)
	{
		// TODO: Complete member initialization
		this.Name = Name;
		this.Index = Index;
		this.Length = Length;
		this.AnimationEvents = animEvent;
	}	
}
public class FBXEventEditor : EditorWindow
{
	
	private static int timelineHash = "timelinecontrol".GetHashCode();

	private const string ANIMATION_FOLDER = "Animation";
	
	AnimationEvent [] apEvents = new AnimationEvent[0];

	bool mEventDirty = false;
	
	[MenuItem("Window/Animation Clip Event Editor")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(FBXEventEditor));
	}
	Object _targetObject;
	Object obj;
	List<AnimationProperties> animationProperties = new List<AnimationProperties>();
	int selectedAnim = 0;
	int selectedAnimLocation = 0;
	Vector2 statePanelScrollPos;
	
	Editor gameObjectEditor;
	void OnGUI()
	{

		EditorGUILayout.BeginHorizontal ();
		obj = EditorGUILayout.ObjectField("FBX Asset", obj, typeof(GameObject), false, GUILayout.Width(position.width * 2 / 3));
		
		if (GUILayout.Button("Reload"))
		{
			_targetObject = null;
		}
		EditorGUILayout.EndHorizontal ();
		
		if (obj != _targetObject)
		{
			_targetObject = obj;
			UpdateAnimationList();
//			Debug.Log (_targetObject.GetInstanceID());
		}
		
//		EditorGUILayout.BeginHorizontal();	GUILayout.Label("Name", GUILayout.Width(100));
//		GUILayout.Label("Length", GUILayout.Width(43));
//		GUILayout.Label("Proc", GUILayout.Width(43));
//		EditorGUILayout.EndHorizontal();

		GUILayout.BeginHorizontal ();
		{
//			GUILayout.FlexibleSpace();
			string [] eventFileList = {"On Prefab", "..."};
			selectedAnimLocation = EditorGUILayout.Popup ("Event file", selectedAnimLocation, eventFileList, GUILayout.Width(position.width * 2 / 3));
			
			if (GUILayout.Button ("Add")) 
			{
				
			}
			if (GUILayout.Button ("Load")) 
			{
				
			}
			EditorGUI.BeginDisabledGroup(selectedAnimLocation != 0);
			{
				if (GUILayout.Button ("Remove")) 
				{
					
				}
			}
			EditorGUI.EndDisabledGroup();
		}
		GUILayout.EndHorizontal ();

		string[] options = new string[animationProperties.Count];
		for (int i = 0; i < animationProperties.Count; i++)
		{
			//			AnimationProperties ap = animationProperties[i];
			//			EditorGUILayout.BeginHorizontal();
			//			GUILayout.Label(ap.Name, GUILayout.Width(100));
			//			GUILayout.Label(ap.Length.ToString(), GUILayout.Width(43));
			//			ap.procTime = EditorGUILayout.Slider(ap.procTime, 0, 1);
			//			EditorGUILayout.EndHorizontal();
			
			//			if (GUILayout.List(animationProperties[i].Name))
			//			{
			//			}
			options[i] = animationProperties[i].Name + " (" + animationProperties[i].AnimationEvents.Length + ")";
		}

		GUILayout.Space(20);
		EditorGUILayout.LabelField ("Animation Clips: " + (animationProperties.Count > 0 ? "" : "No anmimation clips found!"));

//		EditorGUILayout.BeginVertical ();
//		{
//			for (int i = 0; i < options.Length/4; i++)
//			{
//				EditorGUILayout.BeginHorizontal ();
//				for (int j = 0; j < 4; j++)
//				{
//					if (i*4+j < options.Length)
//						GUILayout.Button(options[i*4+j], GUILayout.Width(position.width/4));
//				}
//				GUILayout.EndHorizontal ();
//			}
//		}
//		EditorGUILayout.EndVertical();
		GUILayout.BeginVertical("Box");
		selectedAnim = GUILayout.SelectionGrid(selectedAnim, options, 5);
		EditorGUILayout.EndVertical();

//		if (animationProperties.Count > 0) 
//		{
//			selectedAnim = EditorGUILayout.Popup ("Animation clips", selectedAnim, options);
//
//			AnimationProperties ap = animationProperties[selectedAnim];
//			AnimationEvent [] apEvent = ap.AnimationEvents;
//			if (apEvent.Length > 0) 
//			{
//				for (int i = 0; i < apEvent.Length; i++)
//				{
//					EditorGUILayout.BeginHorizontal();
//					GUILayout.Label(apEvent[i].functionName, GUILayout.Width(300));
//					GUILayout.Label(apEvent[i].time.ToString(), GUILayout.Width(43));
//					EditorGUILayout.EndHorizontal();
//				}
//			}
//			else
//			{
//				EditorGUILayout.LabelField ("No Event!");
//			}
//
//		}
//		else
//		{
//			EditorGUILayout.LabelField ("No Animation clip");
//		}

		if (animationProperties.Count > 0) 
		{
			AnimationProperties ap = animationProperties[selectedAnim];

			if (apEvents != ap.AnimationEvents || mEventDirty)
			{
				if (mEventDirty)
				{
					animationProperties[selectedAnim].AnimationEvents = apEvents;
					mEventDirty = false;
				}
				playbackTime = 0.0f;
				apEvents = ap.AnimationEvents;
			}
			GUILayout.Space(20);
			GUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Current selecterd time:", GUILayout.Width(position.width / 4));
			playbackTime = EditorGUILayout.FloatField(playbackTime, GUILayout.Width(position.width / 4));
			if (GUILayout.Button("Add"))
				AddEvent();
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal(GUILayout.MaxHeight(100)); {
				DrawTimelinePanel(apEvents);
			}
			GUILayout.EndHorizontal();

			GUILayout.BeginVertical ("Box");
			statePanelScrollPos = GUILayout.BeginScrollView (statePanelScrollPos);
//			Debug.Log(statePanelScrollPos);
			for (int i = 0; i < apEvents.Length; i++)
//			foreach(AnimationEvent apevent in apEvents)
			{
				AnimationEvent apevent = apEvents[i];
				GUILayout.BeginHorizontal();

				apevent.time = EditorGUILayout.FloatField(apevent.time, GUILayout.Width(position.width / 14));
				GUI.SetNextControlName(apevent.functionName + "," + apevent.time);
				apevent.functionName = EditorGUILayout.TextArea(apevent.functionName, GUILayout.Width(position.width * 4 / 14));
				apevent.objectReferenceParameter = EditorGUILayout.ObjectField(apevent.objectReferenceParameter, typeof(Object), true, GUILayout.Width(position.width * 2/ 14));
				apevent.intParameter = EditorGUILayout.IntField(apevent.intParameter, GUILayout.Width(position.width / 14));
				apevent.floatParameter = EditorGUILayout.FloatField(apevent.floatParameter, GUILayout.Width(position.width / 14));
				apevent.stringParameter = EditorGUILayout.TextArea(apevent.stringParameter, GUILayout.Width(position.width * 4 / 14));
				if (GUILayout.Button("X")) RemoveEvent(apevent);
				GUILayout.EndHorizontal();
			}
			GUILayout.EndScrollView ();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Save"))
				SaveExportSettings();
			GUILayout.EndHorizontal();

			EditorGUILayout.EndVertical ();
		}

//		if (gameObjectEditor == null)
//			gameObjectEditor = Editor.CreateEditor(obj);
		
//		gameObjectEditor.OnInteractivePreviewGUI(GUILayoutUtility.GetRect(300, 300), EditorStyles.whiteLabel);

		//			GUILayout.Space(20);
		//
		//			if (GUILayout.Button("Set import events"))
		//			{
		//				SaveExportSettings();
		//			}
		//			GUILayout.Space(20);
		//			if (GUILayout.Button("Export Editable"))
		//			{
		//				ExportEditable();
		//			}
		//			
		//			if (GUILayout.Button("Set exported 2 selected"))
		//			{
		//				SetAnimationClips();
		//			}
	}
	
	private void AddEvent()
	{
		AnimationEvent newAE = new AnimationEvent ();
		newAE.time = playbackTime;
		newAE.functionName = "New Event - please edit!!!!";
		List<AnimationEvent> newAEList = new List<AnimationEvent> (apEvents);
		newAEList.Add (newAE);
		apEvents = newAEList.ToArray ();
		mEventDirty = true;
	}

	private void RemoveEvent(AnimationEvent eventToRemove)
	{
		List<AnimationEvent> newAEList = new List<AnimationEvent> (apEvents);
		newAEList.Remove (eventToRemove);
		apEvents = newAEList.ToArray ();
		mEventDirty = true;
	}
	
//	private void ExportEditable()
//	{
//		
//		AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
//		
//		string path = AssetDatabase.GetAssetPath(obj);
//		
//		Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
//		List<AnimationClip> clips = assets.ToList().Where(x => x is AnimationClip && !x.name.Contains("__preview__")).Cast<AnimationClip>().ToList();
//		
//		string clipFolder = System.IO.Path.Combine(System.IO.Directory.GetParent(path).ToString(), ANIMATION_FOLDER);
//		// (!AssetExtentions.DirectoryExists(clipFolder))
//		{
//			Debug.Log("Create folder");
//			AssetDatabase.CreateFolder(System.IO.Directory.GetParent(path).ToString(), ANIMATION_FOLDER);
//		}
		
//foreach (AnimationClip sourceClip in clips)
//{
//	AnimationProperties ap = animationProperties.Where(x => x.Name == sourceClip.name).FirstOrDefault();
//	if (ap != null)
//	{
//		string clipPath = clipFolder + "/" + sourceClip.name + ".anim";
//		Debug.Log(string.Format("Saving:    {0} at {1}", sourceClip.name, clipPath));
//		AnimationClip destClip = AssetDatabase.LoadAssetAtPath(clipPath, typeof(AnimationClip)) as AnimationClip;
//		if (destClip == null)
//		{
//			Debug.Log("new clip");
//			destClip = new AnimationClip();
//			AssetDatabase.CreateAsset(destClip, clipPath);
//		}
//		EditorUtility.CopySerialized(sourceClip, destClip);
//		List<AnimationEvent> events = new List<AnimationEvent>();
//		events.Add(new AnimationEvent() { time = 0, functionName = "onAnimationEvent", intParameter = (int), AnimationEventType.StartstringParameter = destClip.name });
//		events.Add(new AnimationEvent() { time = destClip.length, functionName = "onAnimationEvent", intParameter = (int)AnimationEventType.End, stringParameter = destClip.name });
//		if (ap.procTime != 0)
//		{
//			events.Add(new AnimationEvent() { time = destClip.length * ap.procTime, functionName = "onAnimationEvent", intParameter = (int)AnimationEventType.Proc, stringParameter = destClip.name });
//		}
//		
//		AnimationUtility.SetAnimationEvents(destClip, events.ToArray());
//	}
//	else
//	{
//		Debug.LogWarning("not found settings WTF?!:" + sourceClip.name);
//	}
//	
//}
		
//		Debug.Log(string.Join(",", clips.Select(x => x.name).ToArray()));
//		
//		
//		AssetDatabase.SaveAssets();
//		AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
//		Debug.Log("Export Done");
//		//AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(_targetObject));
//	}
	
	private void SaveExportSettings()
	{
		ModelImporter modelImporter = (ModelImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(_targetObject));
		
		SerializedObject so = new SerializedObject(modelImporter);
		
		SerializedProperty clips = so.FindProperty("m_ClipAnimations");
		List<AnimationEvent[]> animationEvents = new List<AnimationEvent[]>(modelImporter.clipAnimations.Length);
		
//		for (int i = 0; i < modelImporter.clipAnimations.Length; i++)
//		{
//			modelImporter.clipAnimations[i].wrapMode = WrapMode.ClampForever;
//			List<AnimationEvent> events = new List<AnimationEvent>();
			//events.AddRange(GetEvents(clips.GetArrayElementAtIndex(i)));//if we need 2 use old events
//			events.Add(new AnimationEvent() { stringParameter = animationProperties[i].Name, functionName = "onAnimationEvent", time = 0, intParameter = 0 });
//			if (animationProperties[i].procTime != 0)
//				events.Add(new AnimationEvent() { stringParameter = animationProperties[i].Name, functionName = "onAnimationEvent", time = animationProperties[i].procTime, intParameter = 1 });
//			
//			events.Add(new AnimationEvent() { stringParameter = animationProperties[i].Name, functionName = "onAnimationEvent", time = 1, intParameter = 2 });


//			animationEvents.Add(events.ToArray());

//		}
		
		// Make your changes and write them by setting clipAnimations, destroying the events.
		modelImporter.clipAnimations = modelImporter.clipAnimations;
		
		for (int i = 0; i < modelImporter.clipAnimations.Length; i++)
		{
			if (selectedAnim == i)
			{
				SetEvents(clips.GetArrayElementAtIndex(i), apEvents);
			}
		}
		so.SetIsDifferentCacheDirty();
		so.ApplyModifiedProperties();
		AssetDatabase.SaveAssets();
		AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(_targetObject));
	}
	
	private void UpdateAnimationList()
	{
		animationProperties.Clear();
		ModelImporter modelImporter = (ModelImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(_targetObject));

		if (modelImporter != null) 
		{
			SerializedObject so = new SerializedObject (modelImporter);
			SerializedProperty clips = so.FindProperty ("m_ClipAnimations");
			for (int i = 0; i < modelImporter.clipAnimations.Length; i++) {
				ModelImporterClipAnimation iClipAnim = modelImporter.clipAnimations [i];
				animationProperties.Add (new AnimationProperties (iClipAnim.name, i, modelImporter.clipAnimations [i].lastFrame - modelImporter.clipAnimations [i].firstFrame, iClipAnim.events));
			}
		}
		
	}
	
//	private void SetAnimationClips()
//	{
//		Animation targetAnimationComponent = ((GameObject)_targetObject).GetComponent<Animation>();
//		
//		if (targetAnimationComponent == null)
//		{
//			ShowNotification(new GUIContent("No animation selected!", "Select a gameobject with animation component."));
//		}
//		else
//		{
//			WrapMode savedWrapMode = targetAnimationComponent.wrapMode;
//			bool savedPlayAutomaticallyState = targetAnimationComponent.playAutomatically;
//			bool savedAnimatePhysicsState = targetAnimationComponent.animatePhysics;
//			
//			DestroyImmediate(targetAnimationComponent);
//			
//			targetAnimationComponent = Selection.activeGameObject.AddComponent<Animation>();
//			targetAnimationComponent.wrapMode = savedWrapMode;
//			targetAnimationComponent.playAutomatically = savedPlayAutomaticallyState;
//			targetAnimationComponent.animatePhysics = savedAnimatePhysicsState;
//			
//			
//			
//			
//			string path = AssetDatabase.GetAssetPath(obj);
//			Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
//			List<AnimationClip> clips = assets.ToList().Where(x => x is AnimationClip && !x.name.Contains("__preview__")).Cast<AnimationClip>().ToList();
//			
//			string clipFolder = System.IO.Path.Combine(System.IO.Directory.GetParent(path).ToString(), ANIMATION_FOLDER);
//			
//			
//			foreach (AnimationClip sourceClip in clips)
//			{
//				string clipPath = clipFolder + "/" + sourceClip.name + ".anim";
//				AnimationClip destClip = AssetDatabase.LoadAssetAtPath(clipPath, typeof(AnimationClip)) as AnimationClip;
//				targetAnimationComponent.AddClip(destClip, destClip.name);
//				
//				if (targetAnimationComponent.clip == null)
//				{
//					targetAnimationComponent.clip = destClip;
//				}
//			}
//		}
//	}
//	
//	
//	public AnimationEvent[] GetEvents(SerializedProperty sp)
//	{
//		SerializedProperty serializedProperty = sp.FindPropertyRelative("events");
//		AnimationEvent[] array = null;
//		
//		if (serializedProperty != null && serializedProperty.isArray)
//		{
//			int count = serializedProperty.arraySize;
//			array = new AnimationEvent[count];
//			
//			for (int i = 0; i < count; i++)
//			{
//				AnimationEvent animationEvent = new AnimationEvent();
//				
//				SerializedProperty eventProperty = serializedProperty.GetArrayElementAtIndex(i);
//				animationEvent.floatParameter = eventProperty.FindPropertyRelative("floatParameter").floatValue;
//				animationEvent.functionName = eventProperty.FindPropertyRelative("functionName").stringValue;
//				animationEvent.intParameter = eventProperty.FindPropertyRelative("intParameter").intValue;
//				animationEvent.objectReferenceParameter = eventProperty.FindPropertyRelative("objectReferenceParameter").objectReferenceValue;
//				animationEvent.stringParameter = eventProperty.FindPropertyRelative("data").stringValue;
//				animationEvent.time = eventProperty.FindPropertyRelative("time").floatValue;
//				array[i] = animationEvent;
//			}
//		}
//		return array;
//	}
	
	public void SetEvents(SerializedProperty sp, AnimationEvent[] newEvents)
	{
		SerializedProperty serializedProperty = sp.FindPropertyRelative("events");
		if (serializedProperty != null && serializedProperty.isArray && newEvents != null)
		{
			serializedProperty.ClearArray();
			for (int i = 0; i < newEvents.Length; i++)
			{
				AnimationEvent animationEvent = newEvents[i];
				serializedProperty.InsertArrayElementAtIndex(serializedProperty.arraySize);
				
				SerializedProperty eventProperty = serializedProperty.GetArrayElementAtIndex(i);
				eventProperty.FindPropertyRelative("floatParameter").floatValue = animationEvent.floatParameter;
				eventProperty.FindPropertyRelative("functionName").stringValue = animationEvent.functionName;
				eventProperty.FindPropertyRelative("intParameter").intValue = animationEvent.intParameter;
				eventProperty.FindPropertyRelative("objectReferenceParameter").objectReferenceValue = animationEvent.objectReferenceParameter;
				eventProperty.FindPropertyRelative("data").stringValue = animationEvent.stringParameter;
				eventProperty.FindPropertyRelative("time").floatValue = animationEvent.time;
			}
		}
	}
	
	private float playbackTime = 0.0f;

	void DrawTimelinePanel(AnimationEvent [] animEvents) {
		
//		if (!enableTempPreview)
//			playbackTime = eventInspector.GetPlaybackTime();
		
		
		GUILayout.BeginVertical(); {
			
			GUILayout.Space(10);
			
			GUILayout.BeginHorizontal(); {
				
				GUILayout.Space(20);
				
//				playbackTime = Timeline(20);
//				Debug.Log(len);
				playbackTime = Timeline(playbackTime);
				for (int i = 0; i < animEvents.Length; i++)
				{
					DrawAnimEventKey(timelineRect, animEvents[i]);
				}
				GUILayout.Space(10);
				
			}
			GUILayout.EndHorizontal();
			
			GUILayout.FlexibleSpace();
			
//			GUILayout.BeginHorizontal(); {
//				
//				if (GUILayout.Button("Tools")) {
//					GenericMenu menu = new GenericMenu();
//					
//					GenericMenu.MenuFunction2 callback = delegate(object obj) {
//						int id = (int)obj;
//						
//						switch(id)
//						{
//						case 1:
//						{
//							stateClipboard = eventInspector.GetEvents(targetController, selectedLayer, targetState.GetFullPathHash(targetStateMachine));
//							break;
//						}
//							
//						case 2:
//						{
//							eventInspector.InsertEventsCopy(targetController, selectedLayer, targetState.GetFullPathHash(targetStateMachine), stateClipboard);
//							break;
//						}
//							
//						case 3:
//						{
//							controllerClipboard = eventInspector.GetEvents(targetController);
//							break;
//						}
//							
//						case 4:
//						{
//							eventInspector.InsertControllerEventsCopy(targetController, controllerClipboard);
//							break;
//						}
//						}
//					};
					
//					if (targetState == null)
//						menu.AddDisabledItem(new GUIContent("Copy All Events From Selected State"));
//					else
//						menu.AddItem(new GUIContent("Copy All Events From Selected State"), false, callback, 1);
//					
//					if (targetState == null || stateClipboard == null || stateClipboard.Length == 0)
//						menu.AddDisabledItem(new GUIContent("Paste All Events To Selected State"));
//					else
//						menu.AddItem(new GUIContent("Paste All Events To Selected State"), false, callback, 2);
//					
//					if (targetController == null)
//						menu.AddDisabledItem(new GUIContent("Copy All Events From Selected Controller"));
//					else
//						menu.AddItem(new GUIContent("Copy All Events From Selected Controller"), false, callback, 3);
//					
//					if (targetController == null || controllerClipboard == null || controllerClipboard.Count == 0)
//						menu.AddDisabledItem(new GUIContent("Paste All Events To Selected Controller"));
//					else
//						menu.AddItem(new GUIContent("Paste All Events To Selected Controller"), false, callback, 4);
					
					
					
//					menu.ShowAsContext();
//				}
//				
//				GUILayout.FlexibleSpace();
//				
//				if (GUILayout.Button("Add", GUILayout.Width(80))) {
//					MecanimEvent newEvent = new MecanimEvent();
//					newEvent.normalizedTime = playbackTime;
//					newEvent.functionName = "MessageName";
//					newEvent.paramType = MecanimEventParamTypes.None;
//					
//					displayEvents.Add(newEvent);
//					SortEvents();
//					
//					SetActiveEvent(newEvent);
//					
//					MecanimEventEditorPopup.Show(this, newEvent, GetConditionParameters());
//				}
//				
//				if (GUILayout.Button("Del", GUILayout.Width(80))) {
//					DelEvent(targetEvent);
//				}
//				
//				EditorGUI.BeginDisabledGroup(targetEvent == null);
//				
//				if (GUILayout.Button("Copy", GUILayout.Width(80))) {
//					clipboard = new MecanimEvent(targetEvent);
//				}
//				
//				EditorGUI.EndDisabledGroup();
//				
//				EditorGUI.BeginDisabledGroup(clipboard == null);
//				
//				if (GUILayout.Button("Paste", GUILayout.Width(80))) {
//					MecanimEvent newEvent = new MecanimEvent(clipboard);
//					displayEvents.Add(newEvent);
//					SortEvents();
//					
//					SetActiveEvent(newEvent);
//				}
//				
//				EditorGUI.EndDisabledGroup();
//				
//				EditorGUI.BeginDisabledGroup(targetEvent == null);
//				
//				if (GUILayout.Button("Edit", GUILayout.Width(80))) {
//					MecanimEventEditorPopup.Show(this, targetEvent, GetConditionParameters());
//				}
//				
//				EditorGUI.EndDisabledGroup();
//				
//				if (GUILayout.Button("Save", GUILayout.Width(80))) {
//					eventInspector.SaveData();
//				}
//				
//				if (GUILayout.Button("Close", GUILayout.Width(80))) {
//					Close();
//				}
//				
//			}
//			GUILayout.EndHorizontal();
			
		}
		GUILayout.EndVertical();
		
//		if (enableTempPreview) {
//			eventInspector.SetPlaybackTime(tempPreviewPlaybackTime);
//			eventInspector.StopPlaying();
//		}
//		else {
//			eventInspector.SetPlaybackTime(playbackTime);
//		}
		
//		SaveState();
	}

	Rect timelineRect;
	private float Timeline(float time) {
		
		Rect rect = GUILayoutUtility.GetRect(500, 10000, 50, 50);
		
		int timelineId = GUIUtility.GetControlID(timelineHash, FocusType.Native, rect);
		
		Rect thumbRect = new Rect(rect.x + rect.width * time - 5, rect.y + 2, 10, 10);
		
		Event e = Event.current;
		
		switch(e.type) {
		case EventType.Repaint:
			Rect lineRect = new Rect(rect.x, rect.y+10, rect.width, 1.5f);
			DrawTimeLine(lineRect, time);
			GUI.skin.horizontalSliderThumb.Draw(thumbRect, new GUIContent(), timelineId);
			break;
			
		case EventType.MouseDown:
			if (thumbRect.Contains(e.mousePosition)) {
				GUIUtility.hotControl = timelineId;
				e.Use();
			}
			break;
			
		case EventType.MouseUp:
			if (GUIUtility.hotControl == timelineId) {
				GUIUtility.hotControl = 0;
				e.Use();
			}
			break;
			
		case EventType.MouseDrag:
			if (GUIUtility.hotControl == timelineId) {
				
				Vector2 guiPos = e.mousePosition;
				float clampedX = Mathf.Clamp(guiPos.x, rect.x, rect.x + rect.width);
				time = (clampedX - rect.x) / rect.width;
				
				e.Use();
			}
			break;
		}
		timelineRect = rect;
		return time;
		
//		if (displayEvents != null) {
//			
//			foreach(MecanimEvent me in displayEvents) {
//				
//				if (me == targetEvent)
//					continue;
//				
//				DrawEventKey(rect, me);
//			}
			
//			if (targetEvent != null)
//				DrawEventKey(rect, targetEvent);
			
//		}
		
//		return time;
	}

	private void DrawTimeLine(Rect rect, float currentFrame) {
		if (Event.current.type != EventType.Repaint)
		{
			return;
		}
		
		HandleUtilityWrapper.handleWireMaterial.SetPass(0);
		Color c = new Color(1f, 0f, 0f, 0.75f);
		GL.Color(c);
		
		GL.Begin(GL.LINES);
		GL.Vertex3(rect.x, rect.y, 0);
		GL.Vertex3(rect.x + rect.width, rect.y, 0);
		
		GL.Vertex3(rect.x, rect.y+25, 0);
		GL.Vertex3(rect.x + rect.width, rect.y+25, 0);
		
		
		for(int i = 0; i <= 100; i+=1) {
			if (i % 10 == 0) {
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y, 0);
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y + 15, 0);
			}
			else if (i % 5 == 0){
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y, 0);
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y + 10, 0);
			}
			else {
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y, 0);
				GL.Vertex3(rect.x + rect.width*i/100f, rect.y + 5, 0);
			}
		}
		
		c = new Color(1.0f, 1.0f, 1.0f, 0.75f);
		GL.Color(c);
		
		GL.Vertex3(rect.x + rect.width*currentFrame, rect.y, 0);
		GL.Vertex3(rect.x + rect.width*currentFrame, rect.y + 20, 0);
		
		GL.End();
	}
	
	private int hotEventKey = 0;
	private AnimationEvent targetEvent;

	private void SetActiveEvent(AnimationEvent key) {
//			selectedEvent = i;
			targetEvent = key;

	}

	private void DrawAnimEventKey (Rect rect, AnimationEvent key)
	{
		float keyTime = key.time;
		
		Rect keyRect = new Rect(rect.x + rect.width * keyTime - position.width/300, rect.y + 15, position.width/150, 28);
		
		int eventKeyCtrl = key.GetHashCode();
		
		Event e = Event.current;
		
		switch(e.type) {
		case EventType.Repaint:
			Color savedColor = GUI.color;
			
			if (targetEvent == key)
				GUI.color = Color.red;
			else
				GUI.color = Color.green;
			
			GUI.skin.button.Draw(keyRect, new GUIContent(), eventKeyCtrl);
			
			GUI.color = savedColor;
			
			if (hotEventKey == eventKeyCtrl || (hotEventKey == 0 && keyRect.Contains(e.mousePosition))) {
				string labelString = string.Format("{0}@{1}", key.functionName, key.time.ToString("0.0000"));
				Vector2 size = EditorStyles.largeLabel.CalcSize(new GUIContent(labelString));
				
				Rect infoRect= new Rect(rect.x + rect.width * keyTime - size.x/2, rect.y + 50, size.x, size.y);
				EditorStyles.largeLabel.Draw(infoRect, new GUIContent(labelString), eventKeyCtrl);
			}
			break;
			
		case EventType.MouseDown:
			if (keyRect.Contains(e.mousePosition)) {
				
				hotEventKey = eventKeyCtrl;
//				enableTempPreview =true;
//				tempPreviewPlaybackTime = key.normalizedTime;
				GUI.FocusControl (key.functionName + "," + key.time);
				SetActiveEvent(key);
				
//				if (e.clickCount > 1)
//					MecanimEventEditorPopup.Show(this, key, GetConditionParameters());
				
				e.Use();	
			}
			break;
			
		case EventType.MouseDrag:
			if (hotEventKey == eventKeyCtrl) {
				GUI.FocusControl (key.functionName + "," + key.time);
				if (e.button == 0) {
					Vector2 guiPos = e.mousePosition;
					float clampedX = Mathf.Clamp(guiPos.x, rect.x, rect.x + rect.width);
					key.time = (clampedX - rect.x) / rect.width;
//					tempPreviewPlaybackTime = key.normalizedTime;
					SetActiveEvent(key);
				}
				
				e.Use();
			}
			break;
			
		case EventType.MouseUp:
			if (hotEventKey == eventKeyCtrl) {
				
				hotEventKey = 0;
//				enableTempPreview = false;
//				eventInspector.SetPlaybackTime(playbackTime);		// reset to original time
				
//				if (e.button == 1)
//					MecanimEventEditorPopup.Show(this, key, GetConditionParameters());
				
				e.Use();
			}
			break;
		}
	}

//	private void DrawEventKey(Rect rect, MecanimEvent key) {
//		float keyTime = key.normalizedTime;
//		
//		Rect keyRect = new Rect(rect.x + rect.width * keyTime - 3, rect.y+25, 6, 18);
//		
//		int eventKeyCtrl = key.GetHashCode();
//		
//		Event e = Event.current;
//		
//		switch(e.type) {
//		case EventType.Repaint:
//			Color savedColor = GUI.color;
//			
//			if (targetEvent == key)
//				GUI.color = Color.red;
//			else
//				GUI.color = Color.green;
//			
//			GUI.skin.button.Draw(keyRect, new GUIContent(), eventKeyCtrl);
//			
//			GUI.color = savedColor;
//			
//			if (hotEventKey == eventKeyCtrl || (hotEventKey == 0 && keyRect.Contains(e.mousePosition))) {
//				string labelString = string.Format("{0}({1})@{2}", key.functionName, key.parameter, key.normalizedTime.ToString("0.0000"));
//				Vector2 size = EditorStyles.largeLabel.CalcSize(new GUIContent(labelString));
//				
//				Rect infoRect= new Rect(rect.x + rect.width * keyTime - size.x/2, rect.y + 50, size.x, size.y);
//				EditorStyles.largeLabel.Draw(infoRect, new GUIContent(labelString), eventKeyCtrl);
//			}
//			break;
//			
//		case EventType.MouseDown:
//			if (keyRect.Contains(e.mousePosition)) {
//				
//				hotEventKey = eventKeyCtrl;
//				enableTempPreview =true;
//				tempPreviewPlaybackTime = key.normalizedTime;
//				
//				SetActiveEvent(key);
//				
//				if (e.clickCount > 1)
//					MecanimEventEditorPopup.Show(this, key, GetConditionParameters());
//				
//				e.Use();	
//			}
//			break;
//			
//		case EventType.MouseDrag:
//			if (hotEventKey == eventKeyCtrl) {
//				
//				if (e.button == 0) {
//					Vector2 guiPos = e.mousePosition;
//					float clampedX = Mathf.Clamp(guiPos.x, rect.x, rect.x + rect.width);
//					key.normalizedTime = (clampedX - rect.x) / rect.width;
//					tempPreviewPlaybackTime = key.normalizedTime;
//					
//					SetActiveEvent(key);
//				}
//				
//				e.Use();
//			}
//			break;
//			
//		case EventType.MouseUp:
//			if (hotEventKey == eventKeyCtrl) {
//				
//				hotEventKey = 0;
//				enableTempPreview = false;
//				eventInspector.SetPlaybackTime(playbackTime);		// reset to original time
//				
//				if (e.button == 1)
//					MecanimEventEditorPopup.Show(this, key, GetConditionParameters());
//				
//				e.Use();
//			}
//			break;
//		}
//	}
}